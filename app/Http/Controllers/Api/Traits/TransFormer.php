<?php

/**
 * Created by PhpStorm.
 * User: huimingdeng
 * Date: 2019/4/1
 * Time: 22:06
 */
namespace App\Http\Controllers\Api\Traits;

trait TransFormer
{
    /**
     * 转换信息
     * @param $status
     * @param $msg
     * @param $data
     * @return array
     */
	public function TransFormer($status,$msg,$data){
		return [
			'status' => $status,
			'msg' => $msg,
			'data' => $data,
		];
	}
}