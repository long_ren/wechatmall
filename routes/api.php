<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// 测试 API, api 路由需要携带前缀
Route::get('index', function(){
	return 'api';
});
// 定义资源路由	资源控制器的方法不被授予路由控制 eg. xxxController@index 这种方式
// Route::resource('user', 'Api\V1\UserController');
Route::group(['prefx'=>'v1','namespace'=>'Api\V1'], function(){
	// 版本管理
	Route::resource('user', 'UserController');
	Route::resource('user/{create}', 'UserController');
});