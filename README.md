# wechatmall [![Fork me on Gitee](https://gitee.com/huimingdeng/wechatmall/widgets/widget_6.svg)](https://gitee.com/huimingdeng/wechatmall)
下载测试仓库

    git clone git@gitee.com:huimingdeng/wechatmall.git
    git clone https://gitee.com:huimingdeng/wechatmaill.git //当前克隆如果pull文件，则需要的账号密码为 gitee 
	git pull origin master
	git push origin master

[![star](https://gitee.com/huimingdeng/wechatmall/badge/star.svg?theme=dark)](https://gitee.com/huimingdeng/wechatmall/stargazers)
[![fork](https://gitee.com/huimingdeng/wechatmall/badge/fork.svg?theme=dark)](https://gitee.com/huimingdeng/wechatmall/members)

#### 介绍
Laravel 项目实战——微信商城案例。该项目目前阶段开发将仅用于学习用途，非商业用途。

#### 软件架构
软件架构说明--laravel框架开发微信商城。命令`php artisan list` 查看项目使用框架的版本：`Laravel5.7.26`


#### 安装教程

1. `git clone https://gitee.com/huimingdeng/wechatmal.git` 或直接下载 `ZIP` 文件解压安装。
2. 项目需求 `PHP7.1+` 框架 `Laravel5.7.26`
3. 环境低于 `PHP7.1` 版本则项目的 `laravel` 框架无法运行，当然您可以重构项目

#### 使用说明

1. 微信公众号必须有一个（个人可以使用测试号测试）
2. 需要一台带公网IP的服务器，可以购买一台便宜的[腾讯云](https://cloud.tencent.com/ "腾讯云")学生机或 [阿里云](https://www.aliyun.com/ "阿里云")、[青云](https://www.qingcloud.com/ "青云")等云服务器。
3. 目前不支持 composer 安装，若需要支持，则需要在 composer 镜像市场发布，发布教程看 GitHub 上的笔记。

#### 工具 ####

1. `Tinker` 数据交换平台 `Laravel` 自带。 eg. `new App\User` 查看帮助
	1. 创建测试数据 `factory(App\User::class,15)->create()`

P.S. 开发过程创建迁移数据，当前 `laravel` 版本对应的数据库要 `5.7.7` 以上，否则迁移失败，本地开发因数据库版本为 `5.5.53` 导致错误, `phpStudy2018` 升级 [`mysql5.7+`](https://github.com/huimingdeng/hello-world/tree/master/MySQL-learn "mysql5.7.17")

#### API 开发与测试 ####
本地安装 `PostMan` 进行测试 `http://wechatmall.com/api/index` 返回信息 `api`

	// 测试 API, api 路由需要携带前缀
	Route::get('index', function(){
		return 'api';
	});


![简易测试案例](https://i.imgur.com/cj1IBTX.png)

构建特性 `trait` 在存放公用方法，相当于多继承作用。 根据控器其特性构建，`Laravel` 框架使用较多。

`php artisan make:resource UserResource` 创建资源


#### 资源管理器对应操作方法参考 ####
动作：对应的是HTTP请求的action 方式；URI: 为资源路由URL中请求的路径动作；行为：对应资源控制器中的动作方法
<table>
	<thead>
		<tr><th>动作</th><th>URI</th><th>行为</th><th>路由名称</th></tr>
	</thead>
	<tbody>
		<tr>
			<td>`GET`</td>
			<td>`/photos`</td>
			<td>index</td>
			<td>photos.index</td>
		</tr>
		<tr>
			<td>`GET`</td>
			<td>`/photos/create`</td>
			<td>create</td>
			<td>photos.create</td>
		</tr>
		<tr>
			<td>`POST`</td>
			<td>`/photos`</td>
			<td>store</td>
			<td>photos.store</td>
		</tr>
		<tr>
			<td>`GET`</td>
			<td>`/photos/{photo}`</td>
			<td>show</td>
			<td>photos.show</td>
		</tr>
		<tr>
			<td>`GET`</td>
			<td>`/photos/{photo}/edit`</td>
			<td>edit</td>
			<td>photos.edit</td>
		</tr>
		<tr>
			<td>`PUT/PATCH`</td>
			<td>`/photos/{photo}`</td>
			<td>update</td>
			<td>photos.update</td>
		</tr>
		<tr>
			<td>`DELETE`</td>
			<td>`/photos/{photo}`</td>
			<td>destroy</td>
			<td>photos.destroy</td>
		</tr>
	</tbody>
</table>

#### jwt 原理 ####
jwt: Json Web Tokens 简称，JWT 机制是无状态的。

传统方式：

主要是将认证后的用户信息储存在服务器上，比如Session。用户下次请求的时候带上Session Id，然后服务器以此查询用户是否认证过

1. Token 失效问题：

浏览器通过用户名/密码验证获得签名的Token被木马窃取。即用户退出了系统，黑客还是可以根据窃取的Token模拟正常请求登陆服务器，而服务器无法验证，直到过期，中间服务器无法控制。

2. app 类 Token 有效时间：
	1. 如果 app 为新闻/游戏/聊天等长时间用户粘性的，过期时间可设置长期的，eg. 1年
	2. 如果 app 为支付/银行等，token 时效性要很短，eg. 5-15分钟。
